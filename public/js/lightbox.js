window.document.onkeydown = function(e) {
    if (!e) {
        e = event;
    }
    if (e.keyCode == 27) {
        lightbox_close();
    }
}

function lightbox_open(movie, id) {
    var lightBox = "#lightbox-" + id;
    $(lightBox + " .LightBoxVideo").attr("src", movie);

    $(lightBox + " .light").css("display", "block");
    $(lightBox + " .fade").css("display", "block");
    $(lightBox + " .fade").css("opacity", "0.7");

    $(lightBox + " .LightBoxVideo")[0].play();
}

function lightbox_close() {
    var lightBoxVideo = document.getElementById("LightBoxVideo");
    // var lightBox = "#lightbox-" + id;

    $(".light").css("display", "none");
    $(".fade").css("display", "none");
    $(lightBox + " .lightBoxVideo").pause();
}