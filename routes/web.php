<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('clients', 'ClientController');

Route::post('/channels', 'ChannelController@store');
Route::get('/channels/{channel}', 'ChannelController@show')->name('ondemand');
Route::patch('/channels/{channel}', 'ChannelController@update');
Route::delete('/channels/{channel}', 'ChannelController@delete');

Route::post('/livestreams', 'LivestreamController@store');
Route::get('/livestreams/{livestream}', 'LivestreamController@show')->name('livestream');
Route::patch('/livestreams/{livestream}', 'LivestreamController@update');
Route::delete('/livestreams/{livestream}', 'LivestreamController@delete');

Route::post('/livestreams/{livestream}/videos', 'LivestreamVideosController@store');
Route::patch('/livestreams/{livestream}/videos', 'LivestreamVideosController@update');
Route::delete('/livestreams/{livestream}/videos', 'LivestreamVideosController@delete');

Route::post('/channels/{channel}/categories', 'CategoryController@store');
Route::patch('/channels/{channel}/categories', 'CategoryController@update');
Route::delete('/channels/{channel}/categories', 'CategoryController@delete');


Route::post('/categories/{category}/videos', 'VideoController@store');
Route::patch('/categories/{category}/videos', 'VideoController@update');
Route::delete('/categories/{category}/videos', 'VideoController@delete');
Route::get('/categories/{category}/videos', 'VideoController@show');


Route::patch('/styles/{styles}', 'StyleController@update');

Route::patch('/navbarstyles/{navbar}', 'NavbarStyleController@update');

Route::patch('/links/{link}', 'NavbarLinksController@update');

Route::patch('/homepages/{homepage}', 'HomepageController@update');

Route::patch('/socials', 'SocialMediaController@update');

Route::post('/sponsors', 'ClientSponsorController@store');
Route::get('/sponsors/{sponsor}', 'SponsorController@show');
Route::patch('/sponsors', 'SponsorController@update');
Route::delete('/sponsors', 'ClientSponsorController@delete');



Route::get('/', 'ClientController@show')->name('index');

Route::get('/homepage', 'ClientController@show');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
