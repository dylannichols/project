<link rel="stylesheet" href="{{url('css/ui.css')}}" />

<div class="ui-form">
    <div class="row">
        <div class="col"></div>
        <div class="col-xl-4 col-lg-6 col-md-8 col-sm-8 form-container">
            @yield('forms')
        
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="col"></div>
    </div>
</div>

<script>
    $(".close").click(function() {
        $(".ui-form, form").hide();
    });

    $(document).keyup(function(e) {
     if (e.key === "Escape") { 
        $(".ui-form, form").hide();
    }
});
</script>