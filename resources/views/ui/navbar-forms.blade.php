@include('ui.forms.add-channel-form')
@include('ui.forms.add-livestream-page')
@include('ui.forms.edit-navbar-style-form')
@include('ui.forms.edit-navbar-links-form')
@include('ui.forms.edit-page-style-form')
@include('ui.forms.edit-social-media-form')
@include('ui.forms.edit-client-details')
@include('ui.forms.dev-mode-form')
@include('ui.forms.live-mode-form')


