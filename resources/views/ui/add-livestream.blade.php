<div class="swiper-slide add-video-container">
        <i class="fas fa-plus-circle add-video"></i>
        <h1>Add Source</h1>
    </div>
    
<script>
    $(".add-video-container").click(function() {
        $(".form").hide();
        $(".ui-form, .add-livestream-form").show();
    });
</script>