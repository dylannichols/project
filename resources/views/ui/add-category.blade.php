
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('/css/ui.css')}}">

<div class="add-category-container">
    <i class="fas fa-plus-circle add-category"></i>
    <h1>Add Category</h1>
</div>


<script>
    $(".add-category-container").click(function() {
        $(".form").hide();
        $(".ui-form, .add-category-form").show();
    });
</script>