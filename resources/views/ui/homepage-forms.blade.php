@extends('ui.form-template')
@section('forms')
    @include('ui.navbar-forms')
    @include('ui.forms.edit-homepage-text-form')
    @include('ui.forms.edit-homepage-image-form')
@endsection
