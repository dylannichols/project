<form class="edit-navbar-style-form" action="/navbarstyles/{{$navbar->id}}" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Navbar Style</h1>
        
        
        <div class="form-group">
            <label  for="nav-background-colour">Background Colour</label><br>
            <input class="jscolor" name="nav-background-colour" value="{{$navbar->background_colour}}" required>
        </div>
        @error('nav-background-colour')
            <script>
                $("form").hide();
                $(".ui-form, .edit-navbar-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <script src="{{ asset('/js/jscolor.js') }}"></script>
            <label  for="nav-text-colour">Text Colour</label><br>
            <input class="jscolor" name="nav-text-colour" value="{{$navbar->text_colour}}" required>
        </div>
        @error('nav-text-colour')
            <script>
                $("form").hide();
                $(".ui-form, .edit-navbar-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <input type="hidden" id="navbar-font" name="navbar-font" value="{{$navbar->font}}">
            <label>Font</label><br>
            <div id="font-picker-nav" class="nav-font-picker"></div>
        </div>
        <button type="submit" class="btn btn-lg btn-secondary">Add</button>
    </form>
    
 