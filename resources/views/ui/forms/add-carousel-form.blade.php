<form class="add-carousel-form">
    <h1>Create New Category</h1>
    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" name="name" placeholder="Enter channel name here" required>
    </div>
    <button type="submit" class="btn btn-secondary btn-lg">Create</button>
</form>