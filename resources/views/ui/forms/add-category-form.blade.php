{{-- @extends('ui.form-template') @section('form') --}}
<form class="add-category-form" action="/channels/{{$channel->id}}/categories" method="POST">
    @csrf
    <h1>Add New Category</h1>
    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" name="category-name" placeholder="Enter category name here" required>  
    </div>
    @error('category-name')
    <script>
        $("form").hide();
        $(".ui-form, .add-category-form").show();
    </script>
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    <button type="submit" class="btn btn-secondary btn-lg">Add</button>
</form>
{{-- @endsection --}}