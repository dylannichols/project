<form class="edit-livestream-page-form" action="/livestreams/{{$livestream->id}}" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Livestream Page</h1>
        <div class="form-group">
            <label for="livestream-title">Title</label>
            <input class="form-control" type="text" name="livestream-title" id="livestream-title" value="{{$livestream->title}}" required>
        </div>
        @error('livestream-title')
            <script>
                $('form').hide();
                $('.ui-form, .edit-livestream-page-form').show()
                $("#edit-chat").val("{{old('chat')}}")
                $('#livestream-title').val("{{old('livestream-title')}}")

            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="chat">Chat</label>
            <input name="chat" class="form-control" id="edit-chat" type="text" placeholder="Enter chat id here" value="{{$livestream->chat}}" required>
        </div>
        @error('chat')
            <script>
                $('form').hide();
                $('.ui-form, .edit-livestream-page-form').show()
                $('#livestream-title').val("{{old('livestream-title')}}")
                $("#edit-chat").val("{{old('chat')}}")

            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-secondary btn-lg">Edit</button>
        <button class="btn btn-danger btn-lg livestream-page-delete">Delete</button>
    </form>