<form class="edit-channel-form" action="/channels/{{$channel->id}}" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Channel</h1>
        <div class="form-group">
            <label for="name">Name</label>
        <input class="form-control" type="text" name="new-channel-name" id="channel-name" value="{{$channel->name}}" required>
        </div>
        @error('new-channel-name')
        <script>
            $("form").hide();
            $(".ui-form, .edit-channel-form").show();
            $("#channel-name").val("{{old('new-channel-name')}}")
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror  
        <button type="submit" class="btn btn-sec  ondary btn-lg">Edit</button>
        <button class="btn btn-danger btn-lg channel-delete">Delete</button>
    </form>