@if ($channel->categories->count())
<form class="add-video-form" enctype="multipart/form-data" action="/categories/{{$category->id}}/videos" method="POST">
    @csrf
    <h1>Add Video</h1>
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" name="video-title" type="text" placeholder="Enter title here" required value="{{old('video-title')}}">
    </div>
    @error('video-title')
        <script>
            $("form").hide();
            $(".ui-form, .add-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" type="text" name="video-description" rows="2" required>{{old('video-description')}}</textarea>
    </div>
    @error('video-description')
        <script>
            $("form").hide();
            $(".ui-form, .add-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <input type="hidden" id="video-category" name="video-category" value="{{old('video-category')}}">
    <div class="form-group">
        <label for="thumbnail">Thumbnail</label>
        <input type="file" class="form-control-file" name="thumbnail" required>
    </div>
    @error('thumbnail')
        <script>
            $("form").hide();
            $(".ui-form, .add-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="video">Video</label>
        <input type="file" class="form-control-file" name="video" required>
    </div>
    @error('video')
        <script>
            $("form").hide();
            $(".ui-form, .add-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-lg btn-secondary">Add</button>
</form>
@endif
