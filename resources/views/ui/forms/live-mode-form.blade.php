<form class="live-mode-form cancel-form" action="/clients/{{ App\Client::where('url', url('/'))->first()->id}}" method="POST">
    @method('PATCH')
    @csrf
    <h1>Switch to Live Mode?</h1>

    <button type="submit" class="btn btn-secondary btn-lg">Yes</button>
    <button type="button" class="cancel-btn btn btn-red btn-lg">Cancel</button>
</form>
