<form class="sponsor-form">
    <h2>Do you wish to ADD a sponsor or EDIT/DELETE a sponsor?</h2>
    <button class="btn btn-lg btn-secondary add-sponsor">Add</button>
    <button class="cancel-btn btn btn-lg edit-sponsor">Edit/Delete</button>
</form>