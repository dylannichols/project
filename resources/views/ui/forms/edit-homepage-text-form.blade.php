<form class="edit-homepage-text-form" action="/homepages/{{$homepage->id}}" method='POST'>
    @csrf
    @method('PATCH')
    <h1>Edit Homepage Text</h1>
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" type="text" name="title" placeholder="Enter title here" value="{{$homepage->title}}" required>
    </div>
    @error('title')
            <script>
                $('form').hide();
                $('.ui-form, .edit-homepage-text-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" type="text" name="description" rows="2" required>{{$homepage->description}}</textarea>
    </div>
    @error('description')
            <script>
                $('form').hide();
                $('.ui-form, .edit-homepage-text-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="link-one">Link One</label>
        <select class="form-control" name="link-one">
            @if ($homepage->link_one != null)
                <option value="link-one">{{App\Link::find($homepage->link_one)->name}}</option>
            @endif
            <option value="empty">Leave Blank</option>
            @foreach ($linklist as $link)
                @if (!$homepage->haslink($link)) 
                    <option value="{{$link->id}}">{{$link->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="link-two">Link Two</label>
        <select class="form-control" name="link-two">
            @if ($homepage->link_two != null)
                <option value="link-two">{{App\Link::find($homepage->link_two)->name}}</option>
            @endif
            <option value="empty">Leave Blank</option>
            @foreach ($linklist as $link)
                @if (!$homepage->haslink($link)) 
                    <option value="{{$link->id}}">{{$link->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-secondary btn-lg">Edit</button>
</form>