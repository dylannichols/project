<form class="edit-page-style-form" enctype="multipart/form-data" action="/styles/{{$style->id}}" method="POST">
    @csrf
    @method('PATCH')
    <h1>Edit Page Style</h1>
    
    <div class="form-check">
        <input class="form-check-input" type="radio" name="background-style" value="colour" 
        @if (!$style->background_image_used)
            checked
        @endif>
        <label class="form-check-label" for="exampleRadios1">
            Background Colour
        </label>
        <div class="form-group">
            <script src="{{ asset('/js/jscolor.js') }}"></script>
            <input class="jscolor" name="bg-colour" value="{{$style->background}}">
        </div>
        @error('bg-colour')
            <script>
                $("form").hide();
                $(".ui-form, .edit-page-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="background-style"  value="image"
        @if ($style->background_image_used)
            checked
        @endif>
        <label class="form-check-label" for="exampleRadios2">
            Background Image
        </label>
        <div class="form-group">
            <input type="file" class="form-control-file" name="bg-image">
        </div>
        @error('bg-image')
            <script>
                $("form").hide();
                $(".ui-form, .edit-page-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="primary-colour">Accent Colour</label><br>
        <input class="jscolor" name="primary-colour" value="{{$style->primary_colour}}" required>
    </div>
    @error('primary-colour')
            <script>
                $("form").hide();
                $(".ui-form, .edit-page-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label  for="secondary-colour">Footer Colour</label><br>
    <input class="jscolor" name="secondary-colour" value="{{$style->secondary_colour}}" required>
    </div>
    @error('secondary-colour')
            <script>
                $("form").hide();
                $(".ui-form, .edit-page-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label  for="text-colour">Text Colour</label><br>
        <input class="jscolor" name="text-colour" value="{{$style->text_colour}}" required>
    </div>
    @error('text-colour')
            <script>
                $("form").hide();
                $(".ui-form, .edit-page-style-form").show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <input type="hidden" id="font" name="font" value="{{$style->font}}">
        <label>Font</label><br>
        <div id="font-picker" class="page-font-picker"></div>
    </div>
    <button type="submit" class="btn btn-lg btn-secondary">Add</button>
</form>

<script>
    $('.edit-page-style-form').submit(function() {
        var font = fontPicker.getActiveFont().family;
        $('#font').val(font);
    });

    $('.edit-navbar-style-form').submit(function() {
        var font = navPicker.getActiveFont().family;
        $('#navbar-font').val(font);
    });
</script>

