<form class="edit-client-details-form" enctype="multipart/form-data" action="/clients/{{$client->id}}" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Client Details</h1>
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="client-name" id="client-name" value="{{$client->name}}" required>
        </div>
        @error('client-name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="logo">Logo (leave empty to keep same)</label>
            <input type="file" class="form-control-file" name="client-logo">
        </div>
        @error('client-logo')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="icon">Icon (leave empty to keep same)</label>
            <input type="file" class="form-control-file" name="client-icon">
        </div>
        @error('client-icon')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="navbar">Navbar style</label>
            <select class="form-control" id="navbar-style" name="navbar-style">
                @if ($navbar->navbar_type == 1)
                {
                    <option value="1">Centered</option>
                    <option value="2">Default</option>
                    <option value="3">Space around</option>
                    <option value="4">Simple</option>
                }
                @elseif ($navbar->navbar_type == 2)
                {
                    <option value="2">Default</option>
                    <option value="1">Centered</option>
                    <option value="3">Space around</option>
                    <option value="4">Simple</option>
                }
                @elseif ($navbar->navbar_type == 3)
                {
                    <option value="3">Space around</option>
                    <option value="2">Default</option>
                    <option value="1">Centered</option>
                    <option value="4">Simple</option>
                }
                @elseif ($navbar->navbar_type == 4)
                {
                    <option value="4">Simple</option> 
                    <option value="3">Space around</option>
                    <option value="2">Default</option>
                    <option value="1">Centered</option>
                }
                @endif
                
                
            </select>
        </div>
        @error('client-navbar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-secondary btn-lg">Edit</button>
</form>

