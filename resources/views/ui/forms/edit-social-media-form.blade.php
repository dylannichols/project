<form class="edit-social-media-form" action="/socials" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Contact Info</h1>
        <div class="form-group">
            <label for="name">Email</label>
            <input class="form-control" type="text" name="email" id="email"
            @if ($social->email != null)
                value="{{$social->email}}"
            @endif
            >
        </div>
        <div class="form-group">
            <label for="name">Phone</label>
            <input class="form-control" type="text" name="phone" id="phone"
            @if ($social->phone != null)
                value="{{$social->phone}}"
            @endif
            >
        </div>
        <div class="form-group">
            <label for="name">Address</label>
            <input class="form-control" type="text" name="address" id="address" 
            @if ($social->address != null)
                value="{{$social->address}}"
            @endif
            >
        </div>
        <div class="form-group">
            <label for="name">Facebook</label>
            <input class="form-control" type="text" name="facebook" id="facebook" 
            @if ($social->facebook != null)
                value="{{$social->facebook}}"
            @endif
            >
        </div>
        <div class="form-group">
            <label for="name">Twitter</label>
            <input class="form-control" type="text" name="twitter" id="twitter" 
            @if ($social->twitter != null)
                value="{{$social->twitter}}"
            @endif
            >
        </div>
        <div class="form-group">
            <label for="name">Instagram</label>
            <input class="form-control" type="text" name="instagram" id="instagram" 
            @if ($social->instagram != null)
                value="{{$social->instagram}}"
            @endif
            >
        </div>
        <div class="form-group">
            <label for="name">Youtube</label>
            <input class="form-control" type="text" name="youtube" id="youtube"
            @if ($social->youtube != null)
                value="{{$social->youtube}}"
            @endif
            >
        </div>
        <button type="submit" class="btn btn-secondary btn-lg">Edit</button>
</form>
