<form class="edit-sponsor-form"  enctype="multipart/form-data" action="/sponsors/" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Sponsor</h1>
        <input type="hidden" id="video-id" name="video-id" value="video-1">
        <div class="form-group">
            <label class="form-check-label" for="edit-sponsor">
                Edit Sponsor
            </label>
            <div class="form-group">
                <select class="form-control" name="edit-sponsor" id="edit-sponsor" required>
                    @if ($sponsors != null)
                        @foreach ($sponsors as $sponsor)
                            @if ($sponsor->belongsToClient($client->id))
                                <option value="{{$sponsor->id}}">{{$sponsor->name}}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="new-sponsor-name">Name</label>
            <input class="form-control" type="text" id="edit-name" name="new-sponsor-name" placeholder="Enter sponsor name here" required value="{{old('new-sponsor-name')}}">
        </div>
        @error('new-sponsor-name')
            <script>
                $('form').hide();
                $('.ui-form, .edit-sponsor-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="new-sponsor-website">Website</label>
            <input class="form-control" type="text" id="edit-website" name="new-sponsor-website" placeholder="Enter sponsor website here" required value="{{old('new-sponsor-website')}}">
        </div>
        @error('new-sponsor-website')
            <script>
                $('form').hide();
                $('.ui-form, .edit-sponsor-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="new-sponsor-banner">Banner (Leave as empty to keep the same)</label>
            <input type="file" class="form-control-file" name="new-sponsor-banner">
        </div>

        <button type="submit" class="btn btn-lg btn-secondary">Edit</button>
        <button class="btn btn-danger btn-lg sponsor-delete">Delete</button>
    </form>