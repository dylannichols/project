<form class="add-livestream-page-form" action="/livestreams/" method="POST">
    @csrf
        <h1>Create Livestream Page</h1>
        <div class="form-group">
            <label for="livestream-title">Title</label>
            <input name="livestream-title" class="form-control" type="text" placeholder="Enter title here" required value="{{old('livestream-title')}}">
        </div>
        @error('livestream-title')
            <script>
                $('form').hide();
                $('.ui-form, .add-livestream-page-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="chat">Chat</label>
            <input name="chat" class="form-control" type="text" placeholder="Enter chat id here" required value="{{old('chat')}}">
        </div>
        @error('chat')
            <script>
                $('form').hide();
                $('.ui-form, .add-livestream-page-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-lg btn-secondary">Create</button>
    </form>