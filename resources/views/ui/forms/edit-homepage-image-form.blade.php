<form class="edit-homepage-image-form" enctype="multipart/form-data" action="/homepages/{{$homepage->id}}" method='POST'>
    @csrf
    @method('PATCH')
    <h1>Edit Homepage Image</h1>
    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control-file" name="image">
    </div>
    <button type="submit" class="btn btn-secondary btn-lg">Edit</button>
</form>