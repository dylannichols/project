@if ($channel->categories->count())
<form class="delete-video-form" action="/categories/{{$category->id}}/videos" method="POST">
    @csrf
    @method('DELETE')
    <input type="hidden" class="video-id" name="video-id" value="video-1">
    <h2>Are you sure you want to delete this video?</h2>
    <button type="submit" class="btn btn-danger btn-lg">Delete</button>
    <button type="cancel" class="cancel-btn btn btn-lg">Cancel</button>
</form>
@endif
