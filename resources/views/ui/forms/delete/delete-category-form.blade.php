<form class="delete-category-form" action="/channels/{{$channel->id}}/categories/" method="POST">
    @csrf
    @method('DELETE')
    <input type="hidden" class="category-id" name="category-id" value="category-1">
    <h2>Are you sure you want to delete this category?</h2>
    <button type="submit" class="btn btn-danger btn-lg">Delete</button>
    <button type="cancel" class="cancel-btn btn btn-lg">Cancel</button>
</form>
