<form class="delete-sponsor-form" action="/sponsors" method="POST">
    @csrf
    @method('DELETE')
    <input type="hidden" class="sponsor-id" name="sponsor-id" value="sponsor-1">
    <h2>Are you sure you want to delete this sponsorship?</h2>
    <button type="submit" class="btn btn-danger btn-lg">Delete</button>
    <button type="cancel" class="cancel-btn btn btn-lg">Cancel</button>
</form>
