@if ($livestream->videos->count())
<form class="delete-livestream-form" action="/livestreams/{{$livestream->id}}/videos" method="POST">
    @csrf
    @method('DELETE')
    <input type="hidden" class="livestream-id" name="livestream-id" value="livestream-1">
    <h2>Are you sure you want to delete this livestream source?</h2>
    <button type="submit" class="btn btn-danger btn-lg">Delete</button>
    <button type="cancel" class="cancel-btn btn btn-lg">Cancel</button>
</form>
@endif
