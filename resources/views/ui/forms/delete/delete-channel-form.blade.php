<form class="delete-channel-form" action="/channels/{{$channel->id}}" method="POST">
    @csrf
    @method('DELETE')
    <h2>Are you sure you want to delete this channel?</h2>
    <button type="submit" class="btn btn-danger btn-lg">Delete</button>
    <button type="cancel" class="cancel-btn btn btn-lg">Cancel</button>
</form>
