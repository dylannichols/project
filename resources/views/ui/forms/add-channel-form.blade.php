<form class="add-channel-form" action="/channels" method="POST">
    @csrf
    <h1>Create New Channel</h1>
    <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" type="text" name="channel-name" placeholder="Enter channel name here" required value="{{old('channel-name')}}">
    </div>
    @error('channel-name')
        <script>
            $("form").hide();
            $(".ui-form, .add-channel-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror  
    <button type="submit" class="btn btn-secondary btn-lg">Create</button>
</form>
