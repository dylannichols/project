<form class="add-livestream-form"  enctype="multipart/form-data" action="/livestreams/{{$livestream->id}}/videos" method="POST">
    @csrf
    <h1>Add Livestream</h1>
    <div class="form-group">
        <label for="video-title">Title</label>
        <input name="video-title" class="form-control" type="text" placeholder="Enter title here" required value="{{old('video-title')}}">
    </div>
    @error('video-title')
    <script>
        $('form').hide();
        $('.ui-form, .add-livestream-form').show()
    </script>
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="video-description">Description</label>
        <textarea name="video-description" class="form-control" type="text" name="description" rows="2" required>{{old('description')}}</textarea>
    </div>
    @error('video-description')
    <script>
        $('form').hide();
        $('.ui-form, .add-livestream-form').show()
    </script>
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="thumbnail">Thumbnail</label>
        <input name="thumbnail" type="file" class="form-control-file" name="thumbnail" required>
    </div>
    @error('thumbnail')
    <script>
        $('form').hide();
        $('.ui-form, .edit-livestream-page-form').show()
    </script>
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="livestream">Livestream</label>&emsp;<a href="">Click here to access mistserver</a>
        <input name="livestream" class="form-control" type="text" placeholder="Enter mistplayer id here" required>
    </div>
    @error('livestream')
    <script>
        $('form').hide();
        $('.ui-form, .edit-livestream-page-form').show()
    </script>
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="default" value="false">
        <label class="form-check-label" for="default">Make this the default view</label>
    </div>
    <button type="submit" class="btn btn-lg btn-secondary">Add</button>
</form>