<form class="add-sponsor-form" enctype="multipart/form-data" action="/sponsors" method="POST">
    @csrf
    <h1>Add Sponsor</h1>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="sponsor" value="true" checked>
        <label class="form-check-label" for="existing-sponsor">
            Add Existing Sponsor
        </label>
        <div class="form-group">
            <select class="form-control" name="existing-sponsor">
                @if ($sponsors != null)
                    @foreach ($sponsors as $sponsor)
                        @if (!$sponsor->belongsToClient($client->id))
                            <option value="{{$sponsor->id}}">{{$sponsor->name}}</option>
                        @endif
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="sponsor"  value="false">
        <label class="form-check-label" for="existing-sponsor">
            Add New Sponsor
        </label>
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" name="sponsor-name" type="text" placeholder="Enter sponsor name here" value="{{old('sponsor-name')}}">
        </div>
        @error('sponsor-name')
            <script>
                $('form').hide();
                $('.ui-form, .add-sponsor-form').show();
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="website">Website</label>
            <input class="form-control" name="sponsor-website" type="text" placeholder="Enter sponsor website here" value="{{old('sponsor-website')}}">
        </div>
        @error('sponsor-website')
            <script>
                $('form').hide();
                $('.ui-form, .add-sponsor-form').show();
    
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="banner">Banner</label>
            <input type="file" class="form-control-file" name="sponsor-banner" value="{{old('sponsor-banner')}}">
        </div>
        @error('sponsor-banner')
            <script>
                $('form').hide();
                $('.ui-form, .add-sponsor-form').show()
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-lg btn-secondary">Add</button>
</form>

