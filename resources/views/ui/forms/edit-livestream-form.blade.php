<form class="edit-livestream-form" enctype="multipart/form-data" action="/livestreams/{{$livestream->id}}/videos" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Livestream Source</h1>
        <input type="hidden" class="livestream-id" name="livestream-id" id="livestream-id" value="livestream-1">
        <div class="form-group">
            <label for="new-video-title">Title</label>
            <input class="form-control" name="new-video-title" id="title" type="text" placeholder="Enter title here" value="{{old('new-video-title')}}">
        </div>
        @error('new-video-title')
        <script>
            $('form').hide();
            $('.ui-form, .edit-livestream-form').show()
            $('#livestream-id').val("{{old('livestream-id')}}")
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" type="text" id="description" name="new-video-description" rows="2">{{old('new-video-description')}}</textarea>
        </div>
        @error('new-video-description')
        <script>
            $('form').hide();
            $('.ui-form, .edit-livestream-form').show()
            $('#livestream-id').val("{{old('livestream-id')}}")
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="thumbnail">Thumbnail</label>
            <input type="file" class="form-control-file" name="new-thumbnail">
        </div>
        @error('new-thumbnail')
        <script>
            $('form').hide();
            $('.ui-form, .edit-livestream-form').show()
            $('#livestream-id').val("{{old('livestream-id')}}")
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="livestream">Livestream</label>&emsp;<a href="http://localhost:4242" target="_blank">Click here to access mistserver</a>
            <input class="form-control" id="livestream" name="new-livestream" type="text" placeholder="Enter mistplayer id here" value="{{old('new-livestream')}}">
        </div>
        @error('new-livestream')
        <script>
            $('form').hide();
            $('.ui-form, .edit-livestream-form').show()
            $('#livestream-id').val("{{old('livestream-id')}}")
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-check">
            <input type="checkbox" class="form-check-input" name="default" value="false">
            <label class="form-check-label" for="default">Make this the default view</label>
        </div>
        <button type="submit" class="btn btn-lg btn-secondary">Edit</button>
        <button class="btn btn-danger btn-lg livestream-delete">Delete</button>
    </form>