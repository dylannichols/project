<form class="dev-mode-form" action="/clients/{{ App\Client::where('url', url('/'))->first()->id}}" method="POST">
    @method('PATCH')
    @csrf
    <h1>Switch to Development Mode?</h1>
    
    <button type="submit" class="btn btn-secondary btn-lg">Yes</button>
    <button type="submit" class="btn btn-red btn-lg">Cancel</button>
</form>