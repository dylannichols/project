<form class="edit-navbar-links-form" action="/links/{{$links->id}}" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Navbar Links</h1>
    @if ($linklist != null)
        <div class="form-group">
            <label for="link-one">Link One</label>
            <select class="form-control" name="link-one">
                @if ($links->link_one != null)
                    <option value="link-one">{{App\Link::find($links->link_one)->name}}</option>
                @endif
                <option value="empty">Leave Blank</option>
                @foreach ($linklist as $link)
                    @if (!$links->haslink($link)) 
                        <option value="{{$link->id}}">{{$link->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="link-two">Link Two</label>
            <select class="form-control" name="link-two"> 
                @if ($links->link_two != null)
                    <option value="link-two">{{App\Link::find($links->link_two)->name}}</option>
                @endif
                <option value="empty">Leave Blank</option>
                @foreach ($linklist as $link)
                    @if (!$links->haslink($link)) 
                        <option value="{{$link->id}}">{{$link->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
                <label for="link-three">Link Three</label>
                <select class="form-control" name="link-three">
                    @if ($links->link_three != null)
                        <option value="link-three">{{App\Link::find($links->link_three)->name}}</option>
                    @endif
                    <option value="empty">Leave Blank</option>
                    @foreach ($linklist as $link)
                        @if (!$links->haslink($link)) 
                            <option value="{{$link->id}}">{{$link->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                    <label for="link-four">Link Four</label>
                    <select class="form-control" name="link-four">
                        @if ($links->link_four != null)
                            <option value="link-four">{{App\Link::find($links->link_four)->name}}</option>
                        @endif
                        <option value="empty">Leave Blank</option>
                        @foreach ($linklist as $link)
                            @if (!$links->haslink($link)) 
                                <option value="{{$link->id}}">{{$link->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                        <label for="link-five">Link Five</label>
                        <select class="form-control" name="link-five">
                            @if ($links->link_five != null)
                                <option value="link-five">{{App\Link::find($links->link_five)->name}}</option>
                            @endif
                            <option value="empty">Leave Blank</option>
                            @foreach ($linklist as $link)
                                @if (!$links->haslink($link)) 
                                    <option value="{{$link->id}}">{{$link->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                            <label for="link-six">Link Six</label>
                            <select class="form-control" name="link-six">
                                @if ($links->link_six != null)
                                    <option value="link-six">{{App\Link::find($links->link_six)->name}}</option>
                                @endif
                                <option value="empty">Leave Blank</option>
                                @foreach ($linklist as $link)
                                    @if (!$links->haslink($link)) 
                                        <option value="{{$link->id}}">{{$link->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
        @endif
        <button type="submit" class="btn btn-lg btn-secondary">Save</button>
</form>
    
 