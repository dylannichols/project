<form class="edit-category-form" action="/channels/{{$channel->id}}/categories/" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Category</h1>
        <input type="hidden" class="category-id" name="category-id" value="category-1">
        <div class="form-group">
            <label for="name">Name (leave blank to keep name)</label>
            <input class="form-control" type="text" name="new-category-name" id="new-category-name" value="{{old('category-name')}}">
        </div>
        
        @error('new-category-name')
            <script>
                $("form").hide();
                $(".ui-form, .edit-category-form").show();
                $(".category-id").val("{{old('category-id')}}");
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="order">Order (leave blank to keep order)</label>
            <input type="number" class="form-control" name="order" value="{{old('category-name')}}" min="1" max="{{$channel->categories->count()}}">
        </div>
        @error('order')
            <script>
                $("form").hide();
                $(".ui-form, .edit-category-form").show();
                $(".category-id").val("{{old('category-id')}}");
            </script>
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-secondary btn-lg">Edit</button>
        <button class="btn btn-danger btn-lg category-delete">Delete</button>
</form>

