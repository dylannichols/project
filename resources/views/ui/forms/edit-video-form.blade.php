@if ($channel->categories->count())
<form class="edit-video-form"  enctype="multipart/form-data" action="/categories/{{$category->id}}/videos" method="POST">
    @csrf
    @method('PATCH')
        <h1>Edit Video</h1>
        <input type="hidden" class="video-id" name="video-id" value="{{old('video-id')}}">
        <div class="form-group">
            <label for="title">Title</label>
            <input class="form-control" type="text" id="title" name="new-video-title" placeholder="Enter title here" value="{{old('new-video-title')}}">
        </div>
        @error('new-video-title')
        <script>
            $("form").hide();
            $(".ui-form, .edit-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" type="text" id="description" name="new-video-description" rows="2">{{old('new-video-description')}}</textarea>
        </div>
        @error('new-video-description')
        <script>
            $("form").hide();
            $(".ui-form, .edit-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="thumbnail">Thumbnail (Leave as empty to keep the same)</label>
            <input type="file" class="form-control-file" name="new-thumbnail">
        </div>
        @error('new-thumbnail')
        <script>
            $("form").hide();
            $(".ui-form, .edit-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="video">Video (Leave as empty to keep the same)</label>
            <input type="file" class="form-control-file" name="new-video">
        </div>
        @error('new-video')
        <script>
            $("form").hide();
            $(".ui-form, .edit-video-form").show();
        </script>
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-lg btn-secondary">Edit</button>
        <button class="btn btn-danger btn-lg video-delete">Delete</button>
    </form>
@endif