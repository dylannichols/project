<nav class="navbar navbar-expand" >
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            @if ($links->link_one != null)
                <a class="nav-item nav-link" href="{{url('/').App\Link::find($links->link_one)->link}}">{{App\Link::find($links->link_one)->name}}</a>
            @endif
            @if ($links->link_two != null)
                <a class="nav-item nav-link" href="{{url('/').App\Link::find($links->link_two)->link}}">{{App\Link::find($links->link_two)->name}}</a>
            @endif            
            @if ($links->link_three != null)
                <a class="nav-item nav-link" href="{{url('/').App\Link::find($links->link_three)->link}}">{{App\Link::find($links->link_three)->name}}</a>
            @endif
            @if ($links->link_four != null)
                <a class="nav-item nav-link" href="{{url('/').App\Link::find($links->link_four)->link}}">{{App\Link::find($links->link_four)->name}}</a>
            @endif
            @if ($links->link_five != null)
                <a class="nav-item nav-link" href="{{url('/').App\Link::find($links->link_five)->link}}">{{App\Link::find($links->link_five)->name}}</a>
            @endif
            @if ($links->link_six != null)
                <a class="nav-item nav-link" href="{{url('/').App\Link::find($links->link_six)->link}}">{{App\Link::find($links->link_six)->name}}</a>
            @endif
            @include('navigation.nav-dropdown')   
        </div>
    </div>
</nav>