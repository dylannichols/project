
<li class="nav-item dropdown" style="font-family: {{$navbar->font}} !important">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        @if ($user != null)
        @if ($user->isAdmin)
          
          @if ($client->status == 'dev')
            <a class="dropdown-item live-mode">Switch to Live Mode</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item add-channel">Create New Channel</a>
          <a class="dropdown-item add-livestream">Create New Livestream</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item edit-client-details">Edit Client Details</a>
          <a class="dropdown-item edit-contact-info">Edit Contact Info</a>
          <a class="dropdown-item edit-page-style">Edit Page Style</a>
          <a class="dropdown-item edit-navbar-style">Edit Navbar Style</a>
          <a class="dropdown-item edit-navbar-links">Edit Navbar Links</a>
          @endif
          @if ($client->status == 'live')
            <a class="dropdown-item dev-mode">Switch to Development Mode</a>
          @endif
          <div class="dropdown-divider"></div>
          @endif
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                </a>
        @else
        <a class="dropdown-item" href="{{ url('login') }}">
            Login
        </a>
        <a class="dropdown-item" href="{{ url('register') }}">
          Register
      </a>
        @endif
    </div>
</li>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>