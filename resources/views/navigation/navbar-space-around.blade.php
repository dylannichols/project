<nav class="navbar navbar-expand" >
    <div class="navbar-collapse collapse w-100 dual-collapse2 order-1 order-md-0">
        <ul class="navbar-nav nav-fill w-100 text-center">
            <li class="nav-item">
                @if ($links->link_one != null)
                    <a class="nav-link" href="{{url('/').App\Link::find($links->link_one)->link}}">{{App\Link::find($links->link_one)->name}}</a>
                @endif
            </li>
            <li class="nav-item">
                @if ($links->link_two != null)
                    <a class="nav-link" href="{{url('/').App\Link::find($links->link_two)->link}}">{{App\Link::find($links->link_two)->name}}</a>
                @endif
            </li>
            <li class="nav-item">
                @if ($links->link_three != null)
                    <a class="nav-link" href="{{url('/').App\Link::find($links->link_three)->link}}">{{App\Link::find($links->link_three)->name}}</a>
                @endif
            </li>
        </ul>
    </div>
    <div class="mx-auto order-0 order-md-1 position-relative">
        <a class="mx-auto" href="{{url('/')}}">
                <img src="{{asset('storage/'.$style->logo)}}" class="navbar-brand">
            </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 dual-collapse2 order-2 order-md-2">
        <ul class="navbar-nav nav-fill w-100 text-center">
            <li class="nav-item">
                @if ($links->link_four != null)
                    <a class="nav-link" href="{{url('/').App\Link::find($links->link_four)->link}}">{{App\Link::find($links->link_four)->name}}</a>
                @endif
            </li>
            <li class="nav-item">
                @if ($links->link_five != null)
                    <a class="nav-link" href="{{url('/').App\Link::find($links->link_five)->link}}">{{App\Link::find($links->link_five)->name}}</a>
                @endif            
            </li>
            <li class="nav-item">
                @if ($links->link_six != null)
                    <a class="nav-link" href="{{url('/').App\Link::find($links->link_six)->link}}">{{App\Link::find($links->link_six)->name}}</a>
                @endif            
            </li>
            @include('navigation.nav-dropdown')
        </ul>
    </div>
</nav>