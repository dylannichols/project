@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Only administrators can create new client websites. If you believe you shouldn't be seeing this message please e-mail <a href="mailto:dylan@synccity.co.nz">dylan@synccity.co.nz</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
