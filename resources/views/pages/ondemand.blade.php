@extends('layout', ['style' => $style])
@section('content')

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('/css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/carousel.css')}}">
    <link rel="stylesheet" href="{{asset('/css/lightbox-carousel.css')}}">
    @if ($channel->client->status == 'dev')
        <style>
            .channel-header-container, .header-container {
                cursor: pointer;
            }
        </style>
    @endif
    <style>
        .channel-header-blur, .header-blur {
            background-color: #<?= $style->primary_colour?> !important;
            opacity: 0.7;
        }
        .carousel-header, .channel-header {
            font-weight: bold;
            color: #<?= $style->text_colour?> !important;
        }
    </style>

    <script src="{{asset('/js/app.js')}}" type="text/javascript"></script>
    <div class="channel-header-container ui-element">
        <div class="channel-header-blur"></div>
        <h1 class="channel-header">{{$channel->name}}</h1>
    </div>

    @if ($channel->categories->count())
        @foreach ($channel->categories->sortBy('order') as $category)
            @if (!$category->isDeleted)
                @if ($channel->client->status == 'live')
                    @if ($category->videos->count() > 0)
                        @include('includes.lightbox')
                        @include('includes.carousel')
                    @endif
                @else
                    @include('includes.carousel')
                @endif
            @endif
        @endforeach
    @endif
    @if ($channel->client->status == 'dev')
        @include('ui.add-category')
    @endif
    @include('includes.basic-footer')
    {{-- @include('includes.lightbox') --}}
    @include('ui.ondemand-forms', ['style' => $style])

    <script src="{{asset('/js/swiper.min.js')}}"></script>

    @if ($channel->client->status == 'dev')
    <script>


        $(".swiper-slide").click(function(event) {
            if (!$(this).hasClass("add-video-container")) {
                $("form").hide();
                $(".ui-form, .edit-video-form").show();
                $(".edit-video-form .video-id").val($(this).attr('id'));
                $(".delete-video-form .video-id").val($(this).attr('id'));
                $(".edit-video-form #title").val($(this).find(".swiper-slide-title").text());
                $(".edit-video-form #description").text($(this).find(".description-main").text());

            }
        });

        $(".header-container").click(function(event) {
                $("form").hide();
                $(".ui-form, .edit-category-form, .edit-category-form form").show();
                $(".edit-category-form .category-id").val($(this).attr('id'));
                $(".delete-category-form .category-id").val($(this).attr('id'));
                $(".edit-category-form #name").val($(this).find(".carousel-header").text());

        });

        $(".channel-header-container").click(function(event) {
                $("form").hide();
                $(".ui-form, .edit-channel-form").show();
        });

        $(".category-delete").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .delete-category-form").show();
        });

        $(".channel-delete").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .delete-channel-form").show();
        })

        $(".video-delete").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .delete-video-form").show();
        })

      
        $(".add-video-container").click(function() {
            $("form").hide();
            $(".ui-form, .add-video-form").show();
            $(".add-video-form #video-category").val($(this).attr('id'));
        });
    </script>
        @endif

    <script>
        

         $(".cancel-btn").click(function(event) {
            event.preventDefault();
            $(".cancel-form, .ui-form").hide();
        });

        var swiper = new Swiper('.lightbox-swiper-container', {
            slidesPerView: 4,
            spaceBetween: 50,

        });
        var swiper = new Swiper('.main-swiper-container', {
            slidesPerView: 3,
            spaceBetween: 15,
            // autoHeight: true,
            breakpoints: {
                1750: {
                    slidesPerView: 5.5
                },
                1550: {
                    slidesPerView: 5
                },
                1400: {
                    slidesPerView: 4.5
                },
                1180: {
                    slidesPerView: 4
                },
                980: {
                    slidesPerView: 3.5
                },
                780: {
                    slidesPerView: 3
                },
                550: {
                    slidesPerView: 1.5
                },
                380: {
                    slidesPerView: 1.4
                },
                330: {
                    slidesPerView: 1
                }
            }
        });
    </script>
@endsection
