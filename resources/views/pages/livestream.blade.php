@extends ('layout') @section('content')
<link rel="stylesheet" href="{{asset('/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/carousel.css')}}">
{{-- <link rel="stylesheet" href="{{asset('/css/lightbox-carousel.css')}}"> --}}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{ url('/css/livestream.css') }}" />
<script src="{{ url('/js/app.js')}}"></script>

<div class="container">
    <div class="row ui-element">
        <div class="col-lg-9 col-sm-8 col-7 livestream pr-0">
                <div class="mistvideo" id="test_IIxxpYrSndu2">
                        <noscript>
                          <a href="http://localhost:8080/{{$defaultView}}.html" target="_blank">
                            Click here to play this video
                          </a>
                        </noscript>
                        <script src="{{url('/js/mistserver.js')}}"></script>
                        <script>
                          mistserver_open("{{$defaultView}}");
                        </script>
                      </div>
            <div class="livestream-thumbnails livestream-thumbnails-mobile">
                @include('includes.livestream-carousel')
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-4 col-5 pl-0 contact">
        <iframe class="main-chat" seamless scrolling="auto" src='https://minnit.chat/{{$livestream->chat}}?embed&mobiledark' width='100%' height='100%' style='border:none;' allowTransparency='true'></iframe><br><a href='https://minnit.chat/BOOMTVTEST' target='_blank'>Free HTML5 Chatroom powered by Minnit Chat</a>
    </div>
</div>
<div class="row livestream-thumbnails livestream-thumbnails-desktop">
    @include('includes.livestream-carousel')
</div>
</div>

@include('includes.sponsor-footer') 
@include('ui.livestream-forms')

@if ($livestream->client->status == 'dev')
<script>


    $(".swiper-slide").click(function(event) {
        if (!$(this).hasClass("add-video-container")) {
            $("form").hide();
            $(".ui-form, .edit-livestream-form").show();
            $(".edit-livestream-form .livestream-id").val($(this).attr('id'));
            $(".delete-livestream-form .livestream-id").val($(this).attr('id'));
            $(".edit-livestream-form #title").val($(this).find(".swiper-slide-title").text());
            $(".edit-livestream-form #description").text($(this).find(".description-main").text());
            $(".edit-livestream-form #livestream").val($(this).find(".swiper-slide-title").attr('id'));
        }
    });

    $(".livestream-delete").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .delete-livestream-form").show();
    })

    $(".livestream-page-delete").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .delete-livestream-page-form").show();
    })

    $(".mistvideo").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .edit-livestream-page-form").show();
    })

    $("#logoParade").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .sponsor-form").show();
    })

    $(".add-sponsor").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .add-sponsor-form").show();
    })

    $(".edit-sponsor").click(function(event) {
            event.preventDefault();
            $("form").hide();
            $(".ui-form, .edit-sponsor-form").show();
            getSponsor();
    })

    $('#edit-sponsor').on('change', function() {
       getSponsor();
    });

    function getSponsor() {
        var id = $('#edit-sponsor').children('option:selected').val();
        var url = '/sponsors/' + id;

        $.ajax({
            type: 'GET',
            url: url,
            success: function(data) {
                console.log(data);
                $('#edit-name').val(data[0]);
                $('#edit-website').val(data[1])
            }
        });
    }

    $('.sponsor-delete').click(function(event) {
        event.preventDefault();

        $('.sponsor-id').val($('#edit-sponsor').children('option:selected').val());
        $("form").hide();
        $(".ui-form, .delete-sponsor-form").show();
    })
</script>
@endif


@endsection