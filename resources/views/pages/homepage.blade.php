@extends('layout', ['style' => $style])
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{ url('/css/home.css') }}" />
<script type='text/javascript' src='{{ url('/js/app.js') }}'></script>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm d-flex flex-column justify-content-center align-items-center left-column">
        <img class="img-responsive home-img ui-element" src="{{asset('storage/'.$homepage->image)}}" alt="">
        @if ($social->twitter != null || $social->facebook != null || $social->instagram != null || $social->youtube != null)
        <div class="social-links">
            <h3>Follow us on social media:</h3>
            <div class="icons">
                @if ($social->facebook != null )
                    <a href="http://facebook.com/{{$social->facebook }}" target="_blank" class="fa fa-facebook"></a>
                @endif
                @if ($social->twitter != null )
                    <a href="http://twitter.com/{{$social->twitter}}" target="_blank" class="fa fa-twitter"></a>
                @endif  
                @if ($social->instagram != null )
                    <a href="http://instagram.com/{{$social->instagram}}" target="_blank" class="fa fa-instagram"></a>
                @endif 
                @if ($social->youtube != null )
                    <a href="http://youtube.com/{{$social->youtube}}" target="_blank" class="fa fa-youtube"></a>
                @endif 
            </div>  
        </div>
        @endif
        </div>
        <div class="col-sm home-about d-flex flex-column right-column ui-element">
            <h1 class="home-title">
                {{$homepage->title}}
            </h1>
            <h3 class="home-main">
                {{$homepage->description}}

            </h3>
            <div class="home-prompt">
                @if ($homepage->link_one != null)
                    <a href="{{url(App\Link::find($homepage->link_one)->link)}}" class="featured-prompt">{{App\Link::find($homepage->link_one)->name}}</a>
                @endif 
                @if ($homepage->link_two != null) 
                    <a href="{{url(App\Link::find($homepage->link_two)->name)}}" class="ondemand-prompt">{{App\Link::find($homepage->link_two)->name}}</a>
                @endif
            </div>
        </div>
    </div>
    @include('includes.basic-footer')
</div>

@include('ui.homepage-forms')


@if ( $user != null && $user->isAdmin && $client->status == 'dev')
<script type="text/javascript">

$(".right-column").click(function(event) {
    $(".form").hide();
    $(".ui-form, .edit-homepage-text-form").show();

});

$(".left-column").click(function(event) {
    $(".form").hide();
    $(".ui-form, .edit-homepage-image-form").show();

});
</script>
@endif
@endsection
