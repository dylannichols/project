<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
    <link rel="stylesheet" href="{{asset('/css/ui.css')}}">
    <style>
        nav {
            background-color: #<?= $navbar->background_colour?> !important;
        }
        .nav-link {
            color: #<?= $navbar->text_colour?> !important;
            font-family: <?= $navbar->font?> !important;
        }
        .navbar-toggler-icon {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='#<?= $navbar->text_colour?>' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22'/%3E%3C/svg%3E");  
        }
        body {
            color: #<?= $style->text_colour?> !important;
            font-family: <?= $style->font?> !important;
        }
        .home-about {
            /* background-color: #<?= $style->primary_colour?> !important; */
        }
        
     </style>
     @if ($client->status == 'dev')
     <style>
         .ui-element {
             cursor: pointer;
         }

         .ui-element:hover{
             opacity: 0.5;
         }

     </style>
     @endif
     <link rel="icon" href="{{asset('storage/'.$style->icon)}}" type="image/x-icon"/>
     <title>{{$client->name}}</title>

</head>
<body 
style="
    @if (!$style->background_image_used)
    background-color: #{{ $style->background }};
    @else
    background-image: url('{{asset('storage/'.$style->background)}}');
    @endif
     "
     >

    @if ($navbar->navbar_type == 1)
        @include('navigation.navbar-centered')
    @elseif ($navbar->navbar_type == 2)
        @include('navigation.navbar-default')
    @elseif ($navbar->navbar_type == 3)
        @include('navigation.navbar-space-around')
    @elseif ($navbar->navbar_type == 4)
        @include('navigation.navbar-simple')
    @endif

    @yield('content')

    @if ($client->status == 'dev' && $user != null && $user->isAdmin)
    <script>
        // $(".close").click(function() {
        //     $("form").hide();
        // });

        $(".add-channel").click(function() {
            $(".form").hide();
            $(".ui-form, .add-channel-form").show();
        });

        $(".add-livestream").click(function() {
            $(".form").hide();
            $(".ui-form, .add-livestream-page-form").show();
        });

        $(".edit-contact-info").click(function() {
            $(".form").hide();
            $(".ui-form, .edit-social-media-form").show();
        });

        $(".edit-page-style").click(function() {
            $(".form").hide();
            $(".ui-form, .edit-page-style-form").show();
        });

        $(".edit-navbar-style").click(function() {
            $(".form").hide();
            $(".ui-form, .edit-navbar-style-form").show();
        });

        $(".edit-navbar-links").click(function() {
            $(".form").hide();
            $(".ui-form, .edit-navbar-links-form").show();
        });

        $(".edit-client-details").click(function() {
            $(".form").hide();
            $(".ui-form, .edit-client-details-form").show();
        });


    </script>
    
    @endif
    <script src="{{ asset('/js/FontPicker.js') }}"></script>

    <script>
      const fontPicker = new FontPicker(
        "AIzaSyBEtgNncTS72Ncm16VcoGxFmL_W0g7DejU", // Google API key
        "{{$style->font}}", // Default font
        { limit: 100 }, // Additional options
      );

      const navPicker = new FontPicker(
        "AIzaSyBEtgNncTS72Ncm16VcoGxFmL_W0g7DejU", // Google API key
        "{{$navbar->font}}", // Default font
        {
            limit: 100,
            pickerId: "nav",
        }, // Additional options
      );


    </script>
    <script>
        $(".dev-mode").click(function() {
            $(".form").hide();
            $(".ui-form, .dev-mode-form").show();
        });

        $(".live-mode").click(function() {
            $(".form").hide();
            $(".ui-form, .live-mode-form").show();
        });
    </script>
</body>
</html>

