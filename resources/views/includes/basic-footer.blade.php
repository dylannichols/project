<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<style>

.footer-distributed{
	background-color: #343a40!important;
	box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12);
	box-sizing: border-box;
	width: 100%;
	text-align: left;
	font: normal 16px sans-serif;

	padding: 45px 50px;
	margin-top: 80px;
	position: absolute;
	bottom: 0;
	background-color: #<?= $style->secondary_colour?> !important;
}

.footer-distributed .footer-left p, .footer-left a{
	color:  #8f9296;
	font-size: 14px;
	margin: 0;
}



.footer-distributed p.footer-links{
	font-size:18px;
	font-weight: bold;
	color:  #ffffff;
	margin: 0 0 10px;
	padding: 0;
}

.footer-distributed p.footer-links a{
	display:inline-block;
	line-height: 1.8;
	text-decoration: none;
	color:  inherit;
}

.footer-distributed .footer-right{
	float: right;
	margin-top: 6px;
	max-width: 180px;
}

.footer-distributed .footer-right a{
	display: inline-block;
	width: 35px;
	height: 35px;
	background-color:  #33383b;
	border-radius: 2px;

	font-size: 20px;
	color: #ffffff;
	text-align: center;
	line-height: 35px;

	margin-left: 3px;
}

.contact {
	display: inline-block;
	float: right;
	margin-right: 5vw;
	text-align: center;
	color: white;
}

.contact ul {
	list-style: none;
}
.contact li {
line-height: 25px;
}

.contact li a {
	color: white;
}


@media (max-width: 600px) {

	.footer-distributed .footer-left,
	.footer-distributed .footer-right{
		text-align: center;
	}

	.footer-distributed .footer-right{
		float: none;
		margin: 0 auto 20px;
	}

	.footer-distributed .footer-left p.footer-links{
		line-height: 1.8;
	}
}
</style>
		<footer class="footer-distributed">
			

			<div class="footer-right">
				@if ($social->facebook != null )
					<a href="http://facebook.com/{{$social->facebook }}" target="_blank"><i class="fa fa-facebook"></i></a>
				@endif
				@if ($social->twitter != null )
					<a href="http://twitter.com/{{$social->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
				@endif
				@if ($social->instagram != null )
                    <a href="http://instagram.com/{{$social->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
                @endif 
                @if ($social->youtube != null )
                    <a href="http://youtube.com/{{$social->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a>
				@endif 
			</div>

			<div class="contact">
				<ul>
				@if ($social->email != null )
					<li><a href="mailto:{{$social->email}}">{{$social->email}}</a></li>
				@endif
				@if ($social->phone != null )
					<li>{{$social->phone}}</li>
				@endif
				@if ($social->address != null )
					<li>{{$social->address}}</li>
				@endif
			</ul>
			</div>

			<div class="footer-left">

				<p class="footer-links">
					<a href="{{url('/')}}">Home</a>
					@foreach ($linklist as $link)
					·
					<a href="{{$link->link}}">{{$link->name}}</a>			
					@endforeach
				</p>

                <p>SyncroniCity &copy; 2019
                    <a href="#">Privacy Policy</a>
                </p>
			</div>

		</footer>
