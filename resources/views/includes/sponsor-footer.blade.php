<link rel="stylesheet" type="text/css" href="{{ url('/css/smoothDivScroll.css') }}" />

<div id="wrapper" class="ui-element">
        <div id="logoParade">
            @if ($clientsponsor->count() == 0)
                <a href=""> <img src="{{asset('img/sponsor-placeholder.png')}}" alt="Placeholder" height="150" border="0" ></a>
                <a href=""> <img src="{{asset('img/sponsor-placeholder.png')}}" alt="Placeholder" height="150" border="0" ></a>
                <a href=""> <img src="{{asset('img/sponsor-placeholder.png')}}" alt="Placeholder" height="150" border="0" ></a>
            @else
                @foreach ($clientsponsor as $sponsorId)
                    @foreach ($sponsors as $sponsor)
                        @if ($sponsorId->sponsor_id == $sponsor->id && !$sponsorId->isDeleted)
                            <a href="{{$sponsor->link}}" target="_blank"> <img src="{{asset('storage/'.$sponsor->logo)}}" alt="{{$sponsor->name}}" height="150" border="0" /></a>
                        @endif
                    @endforeach
                @endforeach
            @endif
        </div>
    </div>
    
    
    
    @include('includes.jquery')