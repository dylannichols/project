
<div class="carousel">
    <div class="header-container ui-element" id="category-{{$category->id}}">
        <div class="header-blur"></div>
        <h1 class="carousel-header">{{$category->name}}</h1>
    </div>
    <div class="swiper-container main-swiper-container">
        <div class="swiper-wrapper">
            @if ($channel->categories->count())
            @foreach ($category->videos as $video)
            @if (!$video->isDeleted)
            <div class="swiper-slide main-slide ui-element" id="video-{{$video->id}}"
                @if ($channel->client->status == 'live')
                    onclick="lightbox_open('{{asset('storage/'.$video->video)}}', {{$category->id}})"
                @endif
                >
                    <div class="swiper-slide-title">{{$video->title}}</div>
                    <div class="swiper-slide-description">
                        <div class="description-title">{{$video->title}}</div>
                        <div class="description-main">{{$video->description}}</div>
                    </div>
                    <img src="{{asset('storage/'.$video->thumbnail)}}" alt="kitten">
                </div>

        @endif
        @endforeach
        @endif
        @if ($channel->client->status == 'dev')
            @include('ui.add-video')
        @endif
        </div>
    </div>
</div>

