<link rel="stylesheet" type="text/css" href="{{ url('/css/lightbox.css') }}" />

<div id="lightbox-{{$category->id}}">
<div class="light">
        <a class="boxclose" class="boxclose" onclick="lightbox_close();"></a>
        <video class="LightBoxVideo" width="960" controls autoplay="True">
        </video>
        @include('includes.lightbox-carousel')

</div>
<div class="fade" onClick="lightbox_close({{$category->id}});"></div>
</div>
<script src="{{ url('/js/lightbox.js') }}"></script>