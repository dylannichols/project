
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" type="text/css" href="{{ url('/css/app.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
<script src="{{ asset('/js/FontPicker.js') }}"></script>

