

<div class="lightbox-carousel carousel">
    <div class="swiper-container lightbox-swiper-container">
        <div class="swiper-wrapper">
            @if ($livestream->videos->count())
            @foreach ($livestream->videos as $video)
            @if (!$video->isDeleted)
            <div class="swiper-slide main-slide ui-element" id="livestream-{{$video->id}}"
                @if ($livestream->client->status == 'live')
                    onclick="mistserver_open('{{$video->livestream}}')"
                @endif
                >
                    <div class="swiper-slide-title" id="{{$video->livestream}}">{{$video->title}}</div>
                    <div class="swiper-slide-description">
                        <div class="description-title">{{$video->title}}</div>
                        <div class="description-main">{{$video->description}}</div>
                    </div>
                    <img src="{{asset('storage/'.$video->thumbnail)}}" alt="kitten">
                </div>

        @endif
        @endforeach
        @endif
        @if ($livestream->client->status == 'dev')
            @include('ui.add-livestream')
        @endif
        </div>
    </div>

    <script src="{{asset('/js/swiper.min.js')}}"></script>
    <script>
        var swiper = new Swiper('.lightbox-swiper-container', {
            slidesPerView: 4,
            spaceBetween: "3%",
            breakpoints: {
            980: {
                slidesPerView: 3
            },
            780: {
                slidesPerView: 3
            },
            550: {
                slidesPerView: 1.5
            },
            380: {
                slidesPerView: 1.4
            },
            330: {
                slidesPerView: 1
            }
        }
        });
    </script>