<link rel="stylesheet" href="{{asset('/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/carousel.css')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<style>
    .header-container {
        cursor: pointer;
    }
</style>

<div class="carousel carousel2">
    <div class="header-container">
        <div class="header-blur"></div>
        <h1 class="carousel-header">Category Goes Here</h1>
    </div>
    <div class="swiper-container main-swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="swiper-slide-title">Title goes here</div>
                <div class="swiper-slide-description">
                    <div class="description-title">Title goes here</div>
                    <div class="description-main">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde, vero illo possimus excepturi mollitia tempore cumque quia 
                        distinctio ab, suscipit non quam sunt sint pariatur tempora dolor. Eum, vel ut.</div>
                </div>
                <img src="{{asset('/img/400x225.jpg')}}" alt="kitten">
            </div>
            <div class="swiper-slide">
                <div class="swiper-slide-title">Title goes here</div>
                <div class="swiper-slide-description">
                    <div class="description-title">Title goes here</div>
                    <div class="description-main">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Unde, vero illo possimus excepturi mollitia tempore cumque quia 
                        distinctio ab, suscipit non quam sunt sint pariatur tempora dolor. Eum, vel ut.</div>
                </div>
                <img src="{{asset('/img/g400x225.jpg')}}" alt="kitten">
            </div>
            @include('ui.add-video')
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>

<script src="{{asset('/js/swiper.min.js')}}"></script>
<script>
    $(".swiper-slide").click(function(event) {
        if (!$(this).hasClass("add-video-container")) {
            $(".form").hide();
            $(".ui-form, .edit-video-form").show();
        }
    });

    $(".header-container").click(function(event) {
            $(".form").hide();
            $(".ui-form, .edit-category-form").show();

    });

    var swiper = new Swiper('.main-swiper-container', {
        slidesPerView: 3,
        spaceBetween: 15,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1750: {
                slidesPerView: 5.5
            },
            1550: {
                slidesPerView: 5
            },
            1400: {
                slidesPerView: 4.5
            },
            1180: {
                slidesPerView: 4
            },
            980: {
                slidesPerView: 3.5
            },
            780: {
                slidesPerView: 3
            },
            550: {
                slidesPerView: 1.5
            },
            380: {
                slidesPerView: 1.4
            },
            330: {
                slidesPerView: 1
            }
        }
    });
</script>