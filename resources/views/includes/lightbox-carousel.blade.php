
<div class="lightbox-carousel carousel">
    <div class="swiper-container lightbox-swiper-container">
        <div class="swiper-wrapper">
                @if ($channel->categories->count())
                @foreach ($category->videos as $video)
                <div class="swiper-slide main-slide" id="video-{{$video->id}}"
                    @if ($channel->client->status == 'live')
                        onclick="lightbox_open('{{asset('storage/'.$video->video)}}', {{$category->id}})"
                    @endif
                    >
                        <div class="swiper-slide-title">{{$video->title}}</div>
                        <div class="swiper-slide-description">
                            <div class="description-title">{{$video->title}}</div>
                            <div class="description-main">{{$video->description}}</div>
                        </div>
                        <img src="{{asset('storage/'.$video->thumbnail)}}" alt="kitten">
                    </div>


            @endforeach
            @endif
        </div>
        <!-- Add Pagination -->
        </div>
</div>
