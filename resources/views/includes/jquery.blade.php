<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="{{asset('/js/jquery/jquery-ui-1.10.3.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/jquery/jquery.kinetic.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/jquery/jquery.mousewheel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/jquery/jquery.smoothdivscroll-1.3-min.js')}}" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        $("#logoParade").smoothDivScroll({
            autoScrollingMode: "always",
            autoScrollingDirection: "endlessLoopRight",
            autoScrollingStep: 2,
            autoScrollingInterval: 40
        });
        // Logo parade
        $("#logoParade").bind("mouseover", function() {
            $(this).smoothDivScroll("stopAutoScrolling");
        }).bind("mouseout", function() {
            $(this).smoothDivScroll("startAutoScrolling");
        });
    });
</script>