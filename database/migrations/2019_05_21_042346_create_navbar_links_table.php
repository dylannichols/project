<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavbarLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navbar_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('link_one')->unsigned()->nullable();
            $table->bigInteger('link_two')->unsigned()->nullable();
            $table->bigInteger('link_three')->unsigned()->nullable();
            $table->bigInteger('link_four')->unsigned()->nullable();
            $table->bigInteger('link_five')->unsigned()->nullable();
            $table->bigInteger('link_six')->unsigned()->nullable();
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('link_one')->references('id')->on('links')->onDelete('set null');
            $table->foreign('link_two')->references('id')->on('links')->onDelete('set null');
            $table->foreign('link_three')->references('id')->on('links')->onDelete('set null');
            $table->foreign('link_four')->references('id')->on('links')->onDelete('set null');
            $table->foreign('link_five')->references('id')->on('links')->onDelete('set null');
            $table->foreign('link_six')->references('id')->on('links')->onDelete('set null');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navbar_links');
    }
}
