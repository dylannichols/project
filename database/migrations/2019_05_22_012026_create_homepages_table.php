<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('title'); // title for homepage, usually client's company name
            $table->text('description'); // description of client and client's content, usually written by client
            $table->string('image'); // link to image to display on homepage
            // $table->boolean('display_social_links'); // if false then don't display social media links
            $table->bigInteger('link_one')->unsigned()->nullable();
            $table->bigInteger('link_two')->unsigned()->nullable();
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('link_one')->references('id')->on('links')->onDelete('set null');
            $table->foreign('link_two')->references('id')->on('links')->onDelete('set null');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepages');
    }
}
