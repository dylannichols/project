<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('styles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->boolean('background_image_used'); // if true then background will be an image link, otherwise will be a colour
            $table->string('background');
            $table->string('primary_colour'); // primary colour used for styling, may or may not be same as background colour
            $table->string('secondary_colour'); // secondary colour for styling
            $table->string('logo'); // link to client's logo
            $table->string('icon'); // link to client's icon
            $table->string('font');
            $table->string('text_colour'); 
            $table->string('footer_style'); // default -> sponsor footer only on livestream page; sponsor -> sponsor footer every page; footer -> normal footer every page
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('styles');
    }
}
