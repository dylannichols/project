<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// join table for sponsors and clients since clients can share sponsors
class CreateClientSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_sponsors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('sponsor_id')->unsigned();
            $table->boolean('isDeleted')->default(false);
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('sponsor_id')->references('id')->on('sponsors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_sponsors');
    }
}
