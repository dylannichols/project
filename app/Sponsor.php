<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;
use App\ClientSponsor;

class Sponsor extends Model
{
    protected $guarded = [];

    public function belongsToClient($id)
    {
        $client = Client::find($id);
        $clientSponsor = ClientSponsor::where('client_id', $id)->get();

        foreach ($clientSponsor as $sponsor)
        {
            if ($this->id == $sponsor->sponsor_id)
            {
                return true;
            }
        }

        return false;
    }
}
