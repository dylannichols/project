<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livestream extends Model
{
    protected $guarded = [];

    public function videos()
    {
        return $this->hasMany(LivestreamVideos::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
