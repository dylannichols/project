<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Channel;

class Category extends Model
{
    protected $guarded = [];
    
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
