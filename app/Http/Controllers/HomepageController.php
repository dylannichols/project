<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Style;
use App\NavbarStyle;
use App\NavbarLinks;
use App\Link;
use App\Homepage;

class HomepageController extends Controller
{
    public function update($id, Request $request)
    {
        $homepage = Homepage::find($id);
        if ($request->input('title') == null) {
            $path = request()->file('image')->store('photos', 'public');
            $homepage->image = $path;
            $homepage->save();
        } else {
            $validator = $request->validate([
                'title' => 'required|min:3|max:255',
                'description' => 'required|min:20'
            ]);

            $homepage->title = $request->input('title');
            $homepage->description = $request->input('description');


            if ($request->input('link-one') != 'empty' && $request->input('link-one') != 'link-one') {
                $homepage->link_one = $request->input('link-one');
            } else if ($request->input('link-one') == 'empty') {
                $homepage->link_one = null;
            }

            if ($request->input('link-two') != 'empty' && $request->input('link-two') != 'link-two') {
                $homepage->link_two = $request->input('link-two');
            } else if ($request->input('link-two') == 'empty') {
                $homepage->link_two = null;
            }

            $homepage->save();
        }

        return redirect()->route('index');
    }
}
