<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Client;
use App\Style;
use App\NavbarStyle;
use App\NavbarLinks;
use App\Link;
use App\Homepage;
use App\SocialMedia;


class ClientController extends Controller
{
    public function index()
    {
        $client = Client::where('url', url('/'))->first();

        if ($client == null) {
            $user = Auth::user();
            if ($user != null && $user->isAdmin) {
                return view('pages/index');
            } else {
                return view('auth.login');
            }
        } else {
            return view('pages/homepage');
        }
    }

    public function create()
    {
        $user = Auth::user();
        if ($user != null && $user->isAdmin) {
            return view('pages.index');
        } else {
            return view('auth.login');
        }
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|min:3|max:255',
            'logo' => 'required|image',
            'icon' => 'required|image'
        ]);

        $url = url('/');

        $client = Client::create([
            'name' => request('name'),
            'url' => $url,
            'status' => 'dev'
        ]);

        $logoPath = request()->file('logo')->store('photos', 'public');

        $iconPath = request()->file('icon')->store('photos', 'public');

        $style = Style::create([
            'background_image_used' => false,
            'background' => '6c757d',
            'primary_colour' => '000000',
            'secondary_colour' => '343a40',
            'logo' => $logoPath,
            'icon' => $iconPath,
            'font' => 'Open Sans',
            'text_colour' => 'FFF',
            'footer_style' => 'default',
            'client_id' => $client->id,
        ]);

        $navbar = NavbarStyle::create([
            'navbar_type' => request()->input('navbar-type'),
            'background_colour' => '343a40',
            'text_colour' => 'FFF',
            'font' => 'Open Sans',
            'client_id' => $client->id,
        ]);

        $links = NavbarLinks::create([
            'client_id' => $client->id,
        ]);

        $social = SocialMedia::create([
            'client_id' => $client->id,
        ]);

        $linklist = Link::where('client_id', $client->id)->get();

        $homepage = Homepage::create([
            'title' => 'Title goes here',
            'description' => 'Write a brief description about the client to interest potential users',
            'image' => 'photos/homepage-placeholder.png',
            'client_id' => $client->id,
        ]);


        return redirect()->route('index');
    }

    public function show()
    {
        $client = Client::where('url', url('/'))->first();
        $user = Auth::user();

        if ($client == null) {
            if ($user != null && $user->isAdmin) {
                return view('pages/index');
            } else {
                return view('auth.login');
            }
        }

        $style = Style::where('client_id', $client->id)->first();
        $navbar = NavbarStyle::where('client_id', $client->id)->first();
        $links = NavbarLinks::where('client_id', $client->id)->first();
        $linklist = Link::where('client_id', $client->id)->get();
        $homepage = Homepage::where('client_id', $client->id)->first();
        $social = SocialMedia::where('client_id', $client->id)->first();

        return view('pages.homepage', compact(['homepage', 'client', 'style', 'navbar', 'links', 'linklist', 'user', 'social']));
    }

    public function update(Client $client)
    {
        if (request('client-name') == null) {
            if ($client->status === 'dev') {
                $client->dev();
            } else {
                $client->live();
            }
        } else {
            $validator = request()->validate([
                'client-name' => 'required|min:3|max:255',
                'client-logo' => 'nullable|image',
                'client-icon' => 'nullable|image'
            ]);

            $client->name = request('client-name');

            $style = Style::where('client_id', $client->id)->first();

            if (request()->hasFile('client-logo')) {
                $path = request()->file('client-logo')->store('photos', 'public');
                $style->logo = $path;
                $style->save();
            }

            if (request()->hasFile('client-icon')) {
                $path = request()->file('client-icon')->store('photos', 'public');
                $style->icon = $path;
                $style->save();
            }

            $navbar = NavbarStyle::where('client_id', $client->id)->first();
            $navbar->navbar_type = request('navbar-style');

            $client->save();
            $navbar->save();
        }


        return back();
    }
}
