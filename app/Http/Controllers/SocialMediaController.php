<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use App\SocialMedia;

class SocialMediaController extends Controller
{
    public function update(Request $request)
    {

        $client = Client::where('url', url('/'))->first();
        $social = SocialMedia::where('client_id', $client->id)->first();

        if ($request->input('email') != null)
        {
            $social->email = $request->input('email');
        }

        if ($request->input('phone') != null)
        {
            $social->phone = $request->input('phone');
        }

        if ($request->input('address') != null)
        {
            $social->address = $request->input('address');
        }

        if ($request->input('facebook') != null)
        {
            $social->facebook = $request->input('facebook');
        }

        if ($request->input('twitter') != null)
        {
            $social->twitter = $request->input('twitter');
        }

        if ($request->input('instagram') != null)
        {
            $social->instagram = $request->input('instagram');
        }

        if ($request->input('youtube') != null)
        {
            $social->youtube = $request->input('youtube');
        }
        

        $social->save();

        return redirect()->back();

    }
}
