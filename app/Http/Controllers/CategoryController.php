<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Channel;
use Validator;

class CategoryController extends Controller
{
    public function store(Channel $channel, Request $request)
    {
        $validator = $request->validate([
            'category-name' => 'required|min:3|max:255',
        ]);

        $categories = Category::where('channel_id', $channel->id);
        $order = $categories->count() + 1;
        Category::create(['name' => request('category-name'), 'channel_id' => $channel->id, 'order' => $order]);

        return redirect()->back();
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new-category-name' => 'nullable|min:3|max:255',
            'order' => 'nullable'
        ]);


        if ($validator->fails())
        {
            return redirect()->back()
            ->withErrors($validator)->withInput();
        }

        $id = explode("-", $request->input('category-id'))[1];

        $category = Category::find($id);
        if ($request->input('new-category-name') != '')
        {
            $category->name = $request->input('new-category-name');
        }
        if ($request->input('order') != null) 
        {
            $categories = Category::where('channel_id', $category->channel_id);
            foreach ($categories as $cat)
            {
                if ($cat->order >= $request->input('order'))
                {
                    $cat->order = $cat->order + 1;
                    $cat->save();
                }
            }
            $category->order = $request->input('order');
        }
        $category->save();

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $id = explode("-", $request->input('category-id'))[1];
        $category = Category::find($id);
        $category->isDeleted = true;
        $category->save();

        return redirect()->back();
    }

}
