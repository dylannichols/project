<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Video;
use App\Category;

class VideoController extends Controller
{
    public function store(Request $request)
    {
        $validator = $request->validate([
            'video-title' => 'required|min:3|max:225',
            'video-description' => 'required|min:3|max:512',
            'thumbnail' => 'required|image',
            'video' => 'required',
        ]);

        $thumbPath = request()->file('thumbnail')->store('photos', 'public');
        $videoPath = request()->file('video')->store('photos', 'public');
        $category = explode("-", request('video-category'));

        Video::create([
            'title' => request('video-title'),
            'description' => request('video-description'),
            'thumbnail' => $thumbPath,
            'video' => $videoPath,
            'category_id' => $category[2]
        ]);

        return back();
    }


    public function update(Request $request)
    {
        $validator = $request->validate([
            'new-video-title' => 'required|min:3|max:225',
            'new-video-description' => 'required|min:3|max:512',
        ]);

        $id = explode("-", $request->input('video-id'))[1];
        
        $video = Video::find($id);
        $video->title = $request->input('new-video-title');
        $video->description = $request->input('new-video-description');

        if ($request->hasFile('new-thumbnail'))
        {
            $path = request()->file('new-thumbnail')->store('photos', 'public');
            $video->thumbnail = $path;
        }

        if ($request->hasFile('new-video'))
        {
            $path = request()->file('new-video')->store('videos', 'public');
            $video->video = $path;
        }

        $video->save();

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $id = explode("-", $request->input('video-id'))[1];
        $video = Video::find($id);
        $video->isDeleted = true;
        $video->save();

        return redirect()->back();
    }

}
