<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Channel;
use App\NavbarLinks;

// use App\Livestream;

class NavbarLinksController extends Controller
{
    public function update($id, Request $request)
    {
        $links = NavbarLinks::find($id);

        if ($request->input('link-one') != 'empty' && $request->input('link-one') != 'link-one' )
        {
            $links->link_one = $request->input('link-one');
        }
        else if ($request->input('link-one') == 'empty')
        {
            $links->link_one = null;
        }

        if ($request->input('link-two') != 'empty' && $request->input('link-two') != 'link-two' )
        {
            $links->link_two = $request->input('link-two');
        }
        else if ($request->input('link-two') == 'empty')
        {
            $links->link_two = null;
        }

        if ($request->input('link-three') != 'empty' && $request->input('link-three') != 'link-three' )
        {
            $links->link_three = $request->input('link-three');
        }
        else if ($request->input('link-three') == 'empty')
        {
            $links->link_three = null;
        }

        if ($request->input('link-four') != 'empty' && $request->input('link-four') != 'link-four' )
        {
            $links->link_four = $request->input('link-four');
        }
        else if ($request->input('link-four') == 'empty')
        {
            $links->link_four = null;
        }

        if ($request->input('link-five') != 'empty' && $request->input('link-five') != 'link-five' )
        {
            $links->link_five = $request->input('link-five');
        }
        else if ($request->input('link-five') == 'empty')
        {
            $links->link_five = null;
        }

        if ($request->input('link-six') != 'empty' && $request->input('link-six') != 'link-six' )
        {
            $links->link_six = $request->input('link-six');
        }
        else if ($request->input('link-six') == 'empty')
        {
            $links->link_six = null;
        }
        

        $links->save();

        return redirect()->back();
    }
}
