<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\Channel;
use App\NavbarLinks;


class LinkController extends Controller
{
    public function store(Channel $channel = null)
    {
        $name = "";
        $link = "";
        $id = 0;

        if ($channel != null)
        {
            $name = $channel->name;
            $link = '/channels/'.$channel->id;
            $id = $channel->client_id;
        }

        $link = Link::create([
            'name' => $name,
            'link' => $link,
            'client_id' => $id,
        ]);

    }
}
