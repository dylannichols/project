<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use App\Channel;
use App\Client;
use App\Style;
use App\NavbarStyle;
use App\Link;
use App\NavbarLinks;
use App\SocialMedia;

class ChannelController extends Controller
{
    public function store(Request $request)
    {
        $validator = $request->validate([
            'channel-name' => 'required|min:3|max:255',
        ]);

        $client = Client::where('url', url('/'))->first();

        $channel = Channel::create(['name' => request('channel-name'), 'client_id' => $client->id]);

        Link::create([
            'name' => $channel->name,
            'link' => '/channels/'.$channel->id,
            'client_id' => $channel->client_id,
        ]);


        return redirect()->route('ondemand', ['channel' => $channel->id]);
    }

    public function show(Channel $channel)
    {
    
        $client = Client::where('url', url('/'))->first();
        $user = Auth::user();

        if ($client->status == 'dev' && !$user->isAdmin)
        {
            return redirect()->route('index');
        }

        $style = Style::find($client->id);
        $navbar = NavbarStyle::find($client->id);
        $links = NavbarLinks::find($client->id);
        $linklist = Link::where('client_id', $client->id)->get();
        $social = SocialMedia::where('client_id', $client->id)->first();

        if ($channel->isDeleted) {
            return redirect()->route('home');
        }


        return view('pages.ondemand', compact(['channel', 'client', 'style', 'navbar', 'links', 'linklist', 'user', 'social']));
    }

    public function update(Channel $channel, Request $request)
    {
        $validator = $request->validate([
            'new-channel-name' => 'required|min:3|max:255',
        ]);

        $channel->name = $request->input('new-channel-name');
        $channel->save();

        $link = Link::where('link', '/channels/'.$channel->id)->first();
        $link->name = $channel->name;
        $link->save();

        return redirect()->back();
    }

    public function delete(Channel $channel)
    {
        $channel->isDeleted = true;
        $channel->save();

        $link = Link::where('link', '/channels/'.$channel->id)->delete();

        return redirect()->route('home');
    }
}
