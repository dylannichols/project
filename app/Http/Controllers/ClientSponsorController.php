<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sponsor;
use App\Client;
use App\ClientSponsor;

class ClientSponsorController extends Controller
{
    public function store(Request $request)
    {
        $client = Client::where('url', url('/'))->first();

        $type = $request->input('sponsor');

        if ($type == 'true')
        {
            $validator = $request->validate([
                'existing-sponsor' => 'required'
            ]);

            $sponsor = Sponsor::find($request->input('existing-sponsor'));
            
            ClientSponsor::create([
                'sponsor_id' => $sponsor->id,
                'client_id' => $client->id,
            ]);
        }
        else if ($type == 'false')
        {
            $validator = $request->validate([
                'sponsor-name' => 'required|min:3|max:255',
                'sponsor-website' => 'required|url',
                'sponsor-banner' => 'required'
            ]);
            $path = request()->file('sponsor-banner')->store('photos', 'public');

            $sponsor = Sponsor::create([
                'name' => $request->input('sponsor-name'),
                'link' => $request->input('sponsor-website'),
                'logo' => $path,
            ]);

            ClientSponsor::create([
                'sponsor_id' => $sponsor->id,
                'client_id' => $client->id,
            ]);
        }

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $id = $request->input('sponsor-id');
        $sponsor = ClientSponsor::where('sponsor_id', $id)->first();

        $sponsor->isDeleted = true;
        $sponsor->save();

        return redirect()->back();
    }
}
