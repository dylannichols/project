<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Livestream;
use App\Client;
use App\Style;
use App\NavbarStyle;
use App\NavbarLinks;
use App\Link;
use App\Sponsor;
use App\ClientSponsor;
use App\SocialMedia;
use App\LivestreamVideos;

class LivestreamController extends Controller
{
    public function store(Request $request)
    {
        $validator = $request->validate([
            'livestream-title' => 'required|min:3|max:255',
            'chat' => 'required',
        ]);

        $client = Client::where('url', url('/'))->first();

        $livestream = Livestream::create([
            'title' => request('livestream-title'),
            'chat' => request('chat'),
            'default_view' => 0,
            'client_id' => $client->id
        ]);

        Link::create([
            'name' => $livestream->title,
            'link' => '/livestreams/'.$livestream->id,
            'client_id' => $client->id,
        ]);


        return redirect()->route('livestream', ['livestream' => $livestream->id]);
    }

    public function show(Livestream $livestream)
    {
        $client = Client::find($livestream->client_id);
        $user = Auth::user();

        if ($client->status == 'dev' && !$user->isAdmin)
        {
            return redirect()->route('index');
        }
        
        $style = Style::find($client->id);
        $navbar = NavbarStyle::find($client->id);
        $links = NavbarLinks::find($client->id);
        $linklist = Link::where('client_id', $client->id)->get();
        $social = SocialMedia::where('client_id', $client->id)->first();
        $defaultView = LivestreamVideos::find($livestream->default_view)->livestream;

        $clientsponsor = ClientSponsor::where('client_id', $client->id)->get();

        $sponsors = Sponsor::all();

        if ($livestream->isDeleted)
        {
            return redirect()->route('home');
        }


        return view('pages.livestream', compact(['livestream', 'client', 'style', 'navbar', 'links', 'linklist', 'sponsors', 'clientsponsor', 'user', 'social', 'defaultView']));
    }

    public function update($id, Request $request)
    {
        $validator = $request->validate([
            'livestream-title' => 'required|min:3|max:255',
            'chat' => 'required',
        ]);

        $livestream = Livestream::find($id);

        $livestream->title = $request->input('livestream-title');
        $livestream->chat = $request->input('chat');

        $livestream->save();

        $link = Link::where('link', '/livestreams/'.$id)->first();
        $link->name = $livestream->title;
        $link->save();

        return redirect()->back();
    }

    public function delete(Livestream $livestream)
    {
        $livestream->isDeleted = true;
        $livestream->save();

        $link = Link::where('link', '/livestreams/'.$livestream->id)->delete();

        return redirect()->route('home');
    }
}
