<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sponsor;

class SponsorController extends Controller
{
    public function show($id) {
        $sponsor = Sponsor::find($id);
        $name = $sponsor->name;
        $link = $sponsor->link;
        return [$name, $link];
    }

    public function update(Request $request)
    {
        $validator = $request->validate([
            'new-sponsor-name' => 'required|min:3|max:255',
            'new-sponsor-website' => 'required|url'
        ]);
        $id = $request->input('edit-sponsor');
        $sponsor = Sponsor::find($id);

        $sponsor->name = $request->input('new-sponsor-name');
        $sponsor->link = $request->input('new-sponsor-website');

        if ($request->hasFile('new-sponsor-banner'))
        {
            $path = request()->file('new-sponsor-banner')->store('photos', 'public');
            $sponsor->logo = $path;
        }

        $sponsor->save();

        return redirect()->back();
    }
}
