<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Style;
use App\Client;

class StyleController extends Controller
{
    
    public function update($id, Request $request)
    {
        $validator = $request->validate([
            'primary-colour' => ['required', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
            'secondary-colour' => ['required', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
            'text-colour' => ['required', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
        ]);

        $style = Style::where('id', $id)->first();
        $backgroundStyle = $request->input('background-style');

        if ($backgroundStyle == 'colour')
        {
            $style->background_image_used = false;
            $style->background = $request->input('bg-colour');
            $validator = $request->validate([
                'bg-colour' => ['required', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
            ]);
        }
        
        if ($backgroundStyle == 'image')
        {
            $style->background_image_used = true;
            $path = request()->file('bg-image')->store('photos', 'public');
            $style->background = $path;
            $validator = $request->validate([
                'bg-image' => 'required|image',
            ]);
        }

        $style->footer_style = 'default';
        $style->primary_colour = $request->input('primary-colour');
        $style->secondary_colour = $request->input('secondary-colour');
        $style->text_colour = $request->input('text-colour');
        $style->font = $request->input('font');

        $style->save();

        return redirect()->back();
    }
}
