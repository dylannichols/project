<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NavbarStyle;

class NavbarStyleController extends Controller
{
    public function update($id, Request $request)
    {
        $validator = $request->validate([
            'nav-background-colour' =>  ['required', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
            'nav-text-colour' => ['required', 'regex:/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
        ]);

        $navbar = NavbarStyle::where('id', $id)->first();

        $navbar->background_colour = $request->input('nav-background-colour');
        $navbar->text_colour = $request->input('nav-text-colour');
        $navbar->font = $request->input('navbar-font');

        $navbar->save();

        return redirect()->back();
    }
}
