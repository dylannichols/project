<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Livestream;
use App\LivestreamVideos;

class LivestreamVideosController extends Controller
{
    public function store($id, Request $request)
    {
        $validator = $request->validate([
            'video-title' => 'required|min:3|max:255',
            'video-description' => 'required|min:3|max:511',
            'thumbnail' => 'required|image',
            'livestream' => 'required',
        ]);

        $livestream = Livestream::find($id);

        $thumbPath = request()->file('thumbnail')->store('photos', 'public');

        $video = LivestreamVideos::create([
            'title' => request('video-title'),
            'description' => request('video-description'),
            'thumbnail' => $thumbPath,
            'livestream' => request('livestream'),
            'livestream_id' => $livestream->id,
        ]);

        if (request('default'))
        {
            $livestream->default_view = $video->id;
            $livestream->save();
        }

        return back();
    }

    public function update(Request $request)
    {
        $validator = $request->validate([
            'new-video-title' => 'required|min:3|max:255',
            'new-video-description' => 'required|min:3|max:511',
            'new-livestream' => 'required',
        ]);


        $id = explode("-", $request->input('livestream-id'))[1];
        
        $video = LivestreamVideos::find($id);
        $video->title = $request->input('new-video-title');
        $video->description = $request->input('new-video-description');
        $video->livestream = $request->input('new-livestream');

        $livestream = Livestream::find($video->livestream_id);

        if (request('default'))
        {
            $livestream->default_view = $video->id;
            $livestream->save();
        }

        if ($request->hasFile('new-thumbnail'))
        {
            $path = request()->file('new-thumbnail')->store('photos', 'public');
            $video->thumbnail = $path;
        }

        $video->save();

        return redirect()->back();

    }

    public function delete(Request $request)
    {
        $id = explode("-", $request->input('livestream-id'))[1];
        $video = LivestreamVideos::find($id);
        $video->isDeleted = true;
        $video->save();

        return redirect()->back();
    }
}
