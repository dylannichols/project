<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = [];

    public function channels()
    {
        return $this->hasMany(Channel::class);
    }

    public function live()
    {
        $this->status = 'dev';
        $this->save();
    }

    public function dev()
    {
        $this->status = 'live';
        $this->save();
    }
}
