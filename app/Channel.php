<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Client;
use App\Category;

class Channel extends Model
{
    protected $guarded = [];
    
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }


}
