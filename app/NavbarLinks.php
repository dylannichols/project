<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NavbarLinks extends Model
{
    protected $guarded = [];

    public function links()
    {
        return $this->hasMany(Link::class);
    }

    public function haslink(Link $link)
    {
        if ($this->link_one == $link->id)
        {
            return true;
        }
        if ($this->link_two == $link->id)
        {
            return true;
        }
        if ($this->link_three == $link->id)
        {
            return true;
        }
        if ($this->link_four == $link->id)
        {
            return true;
        }
        if ($this->link_five == $link->id)
        {
            return true;
        }
        if ($this->link_six == $link->id)
        {
            return true;
        }

        return false;
    }
}
